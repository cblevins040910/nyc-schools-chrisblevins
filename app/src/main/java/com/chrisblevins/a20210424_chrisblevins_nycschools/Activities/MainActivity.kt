package com.chrisblevins.a20210424_chrisblevins_nycschools.Activities

/***
 * By Chris Blevins
 * Created on 2021/04/24
 */

import android.app.Activity
import android.content.Intent
import android.graphics.Rect
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.chrisblevins.a20210424_chrisblevins_nycschools.Models.SchoolData
import com.chrisblevins.a20210424_chrisblevins_nycschools.R
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import java.io.Serializable


class MainActivity : AppCompatActivity() {

    private lateinit var listView: ListView
    private lateinit var searchView: SearchView
    private lateinit var adapter: ArrayAdapter<*>
    private lateinit var jsonList: List<SchoolData> // Used to filter json when School Name is selected
    private lateinit var schoolNameList: ArrayList<String> // Used for populating ListView with only School Name


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listView = schoolListView
        searchView = schoolSearchView
        schoolNameList = ArrayList()

        populateListView()

        // SETUP ListView Item Clicked action
        listView.setOnItemClickListener { _, _, position, _ ->
            gotoActivity(position)
        } // clickListener

    } // onCreate


    // GO To New Activity
    private fun gotoActivity(position: Int) {
        val item = adapter.getItem(position) // The item that was clicked

        if (item != null) {
            val jsonObj = jsonList.find { it.SCHOOL_NAME == item.toString() } // Get individual json object from the item selected
            val intent = Intent(this, SchoolViewActivity::class.java) // Select new activity to go to
            intent.putExtra("schoolData", jsonObj as Serializable) // Parse json object to new Activity
            startActivity(intent) // Go to new activity
        }

    } // gotoActivity


    // Setup ListView
    private fun populateListView() {
        // Use GSON(ad on dependency) to retrieve JSON data
        val gson = GsonBuilder().create()
        val jsonFileName = retrieveJson("nyc-schools.json")
        jsonList = gson.fromJson(jsonFileName , Array<SchoolData>::class.java).toList()

        // append schoolNameList
        for (school in jsonList) {
            schoolNameList.add(school.SCHOOL_NAME.toString())
        }

        // Setup Adapter and Populate List View
        adapter = ArrayAdapter(this, R.layout.custom_list_view, R.id.listTextView, schoolNameList)
        listView.adapter = adapter

        setupSearchView()

    } // populateListView


    // Setup SearchView
    private fun setupSearchView() {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (schoolNameList.contains(query)) {
                    adapter.filter.filter(query)
                } else {
                    Toast.makeText(this@MainActivity, "0 Results", Toast.LENGTH_LONG).show()
                }
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                adapter.filter.filter(newText)
                return false
            }

        })

    } // setupSearchView


    // Get JSON data from assets(directory) Suppress fileName as it is always nyc-schools.json
    @Suppress("SameParameterValue")
    private fun retrieveJson(fileName: String): String? {
        val jsonString: String

        try {
            jsonString = this.assets.open(fileName).bufferedReader().use { it.readText() }
        }
        catch (ioException: IOException) {
            ioException.printStackTrace()

            Toast.makeText(this, "An error has occurred", Toast.LENGTH_SHORT).show()

            return null
        }

        return jsonString

    } // retrieveJson


    // CLEAR TEXT WHEN SCREEN IS TOUCHED
    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus

            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)

                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }

        return super.dispatchTouchEvent(event)

    } // dispatchTouchEvent

} // Class