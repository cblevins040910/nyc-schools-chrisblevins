package com.chrisblevins.a20210424_chrisblevins_nycschools.Activities

/***
 * By Chris Blevins
 * Created on 2021/04/24
 */

import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.chrisblevins.a20210424_chrisblevins_nycschools.Models.SchoolData
import com.chrisblevins.a20210424_chrisblevins_nycschools.R
import kotlinx.android.synthetic.main.activity_school_view.*
import org.json.JSONException


class SchoolViewActivity : AppCompatActivity() {

    private lateinit var mView: ConstraintLayout
    private lateinit var schoolLbl: TextView
    private lateinit var dbnLabel: TextView
    private lateinit var numSatLabel: TextView
    private lateinit var avgReadLabel: TextView
    private lateinit var avgMathLabel: TextView
    private lateinit var avgWriteLabel: TextView
    private lateinit var schoolDataList: SchoolData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school_view)

        mView = svMainCon
        mView.visibility = View.GONE
        schoolLbl = schoolNameLbl
        dbnLabel = dbnLbl
        numSatLabel = numSatTesters
        avgReadLabel = avgReadingLbl
        avgMathLabel = avgMathLbl
        avgWriteLabel = aveWritingLbl

        // Get intent list of selected school data
        try {
            schoolDataList = intent.extras?.get("schoolData") as SchoolData
            populateData()
        }
        catch (e: JSONException) {
            Toast.makeText(this, "An error occurred", Toast.LENGTH_SHORT).show()
            this.finish()
            e.printStackTrace()
            return
        }

    } // onCreate

    private fun populateData() {
        schoolLbl.text = schoolDataList.SCHOOL_NAME
        dbnLabel.text = schoolDataList.DBN
        numSatLabel.text = schoolDataList.Num_of_SAT_Test_Takers
        avgReadLabel.text = schoolDataList.Avg_Reading_Score
        avgMathLabel.text = schoolDataList.Avg_Math_Score
        avgWriteLabel.text = schoolDataList.Avg_Writing_Score
        mView.visibility = View.VISIBLE

    } // populateData

} // Class