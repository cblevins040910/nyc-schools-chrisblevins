package com.chrisblevins.a20210424_chrisblevins_nycschools.Models

/***
 * By Chris Blevins
 * Created on 2021/04/24
 */

import java.io.Serializable


data class SchoolData (
    var DBN: String,
    var SCHOOL_NAME: String?,
    var Num_of_SAT_Test_Takers: String?,
    var Avg_Reading_Score: String?,
    var Avg_Math_Score: String?,
    var Avg_Writing_Score: String?
) : Serializable